import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { Button, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';
import moment from 'moment';
import Chart from './Chart';

export default function App() {
  const [description, setDescription] = useState('');
  const [amount, setAmount] = useState('');
  const [total, setTotal] = useState(0);
  const [data, setData] = useState([
    {[moment()]: 2000},
    {[moment().subtract(1, 'days')]: 2000},
    {[moment().subtract(2, 'days')]: 2000},
  ])

  const [gigs, setGigs] = useState([
    {
      description: 'Freelance with Twitter',
      amount: '1000',
      timestamp: new Date()
    }
  ]);

  const getDates = () => data.map(pair => Object.keys(pair)[0]);
  const getAmounts = () => data.map(pair => Object.values(pair)[0]);

  console.log('DEBUG', data);
  console.log('DATES', getDates());
  console.log('AMOUNTS', getAmounts());

  useEffect(() => {
    setTotal(gigs.reduce((total, gig) => total + Number(gig.amount), 0));
    console.log(data)
  }, [gigs])

  const addGigs = () => {
    setGigs([...gigs,{
      description,
      amount,
      timestamp: new Date()
    }]);
    setDescription('');
    setAmount('');
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.topbar}>Gigs</Text>
      <Chart date={getDates()} amount={getAmounts()}/>
      <Text> Total Income: $ {total} </Text>
      <TextInput 
        style={styles.textbox}
        value={description}
        placeholder="Enter job description"
        onChangeText={setDescription}
      />
      <TextInput 
        style={styles.textbox}
        keyboardType="number-pad"
        value={amount}
        placeholder="Enter amount earned ($)"
        onChangeText={setAmount}
      />

      <Button 
        title="Add Gig ✔️"
        disabled={(!description && !amount)}
        onPress={addGigs}
      />
      
      {
        gigs.map((gig, i) => (
          <View key={i}>
            <Text> {gig.description} </Text>
            <Text> {gig.amount} </Text>
          </View>
        ))
      }
    
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
    marginTop: 30,
  },
  topbar: {
    backgroundColor: 'red',
    height: 40,
    fontSize: 20
  },
  textbox: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    margin: 20,
    padding: 5
  }
});
