import { StatusBar } from 'expo-status-bar';
import React, {useEffect, useState} from 'react';
import { Button, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';
import Chart from './Chart';

export default function App() {
  const [description, setDescription] = useState('');
  const [amount, setAmount] = useState('');
  const [date, setDate] = useState('Jan');
  const [jan, setjan] = useState(0)
  const [feb, setfeb] = useState(0)
  const [mar, setmar] = useState(0)
  const [total, setTotal] = useState(0);
  const [gigs, setGigs] = useState([
    {
      description: 'Freelance with Twitter',
      amount: '1000',
    }
  ]);

  
  const data = [jan,feb,mar];
  useEffect(() => {
    setTotal(gigs.reduce((total, gig) => total + Number(gig.amount), 0));
    setjan(jan);
    setfeb(feb);
    setmar(mar);
    console.log(data)
  }, [gigs])

  const addGigs = () => {
    setGigs([...gigs,{
      description,
      amount,
      date
    }]);
    if(date === 'Jan') 
      setjan(jan +  Number(amount));
    if(date === "Feb")
      setfeb(feb +  Number(amount));
    if(date === 'Mar')
      setmar(mar + Number(amount));

    setDescription('');
    setAmount('');
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.topbar}>Gigs</Text>
      <Chart chartdata={data}/>
      <Text> Total Income: $ {total} </Text>
      <TextInput 
        style={styles.textbox}
        value={description}
        placeholder="Enter job description"
        onChangeText={setDescription}
      />
      <TextInput 
        style={styles.textbox}
        keyboardType="number-pad"
        value={amount}
        placeholder="Enter amount earned ($)"
        onChangeText={setAmount}
      />
      <TextInput 
        style={styles.textbox}
        value={date}
        placeholder="Enter month of earning"
        onChangeText={setDate}
      />
      <Button 
        title="Add Gig ✔️"
        disabled={(!description && !amount)}
        onPress={addGigs}
      />
      
      {
        gigs.map((gig, i) => (
          <View key={i}>
            <Text> {gig.description} </Text>
            <Text> {gig.amount} </Text>
          </View>
        ))
      }
    
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
    marginTop: 30,
  },
  topbar: {
    backgroundColor: 'red',
    height: 40,
    fontSize: 20
  },
  textbox: {
    height: 40,
    borderColor: 'red',
    borderWidth: 1,
    margin: 20,
    padding: 5
  }
});
